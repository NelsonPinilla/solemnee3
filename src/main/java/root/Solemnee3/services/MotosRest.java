/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.Solemnee3.services;

import com.mycompany.solemnee3.dao.MotosJpaController;
import com.mycompany.solemnee3.dao.exceptions.NonexistentEntityException;
import com.mycompany.solemnee3.entity.Motos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Draggnel
 */
@Path("motos")
public class MotosRest {
    
 @GET
 @Produces(MediaType.APPLICATION_JSON)
    
    public Response listarMotos(){
        MotosJpaController dao=new MotosJpaController();
        List<Motos> motos= dao.findMotosEntities();
        return Response.ok(200).entity(motos).build();
    }
    
 @POST
 @Produces(MediaType.APPLICATION_JSON)
  public Response crear( Motos moto ){
             MotosJpaController dao=new MotosJpaController();
     try {
         dao.create(moto);
     } catch (Exception ex) {
         Logger.getLogger(MotosRest.class.getName()).log(Level.SEVERE, null, ex);
     }
             return Response.ok(200).entity(moto).build();
     
  }
  @PUT
  @Produces (MediaType.APPLICATION_JSON)
  public Response actualizar( Motos moto ){
     MotosJpaController dao=new MotosJpaController();
     try {
         dao.edit(moto);
     } catch (Exception ex) {
         Logger.getLogger(MotosRest.class.getName()).log(Level.SEVERE, null, ex);
     }
     return Response.ok(200).entity(moto).build();
  }
  @DELETE
  @Path("/{ideliminar}")
  @Produces (MediaType.APPLICATION_JSON)
  public Response eliminar(@PathParam("ideliminar") String ideliminar){
      MotosJpaController dao=new MotosJpaController();
     try {
         dao.destroy(ideliminar);
     } catch (NonexistentEntityException ex) {
         Logger.getLogger(MotosRest.class.getName()).log(Level.SEVERE, null, ex);
     }
      return Response.ok("Moto Eliminada").build();
  }
  @GET
  @Path("/{idConsultar}")
  @Produces (MediaType.APPLICATION_JSON)
  public Response consultarPorId(@PathParam("idConsultar") String idConsultar){
  MotosJpaController dao=new MotosJpaController();
   Motos moto= dao.findMotos(idConsultar);
   return Response.ok(200).entity(moto).build();
  }
  }
          
