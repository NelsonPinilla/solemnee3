/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solemnee3.dao;

import com.mycompany.solemnee3.dao.exceptions.NonexistentEntityException;
import com.mycompany.solemnee3.dao.exceptions.PreexistingEntityException;
import com.mycompany.solemnee3.entity.Motos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Draggnel
 */
public class MotosJpaController implements Serializable {

    public MotosJpaController() {
 
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_MOTOS");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Motos motos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(motos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMotos(motos.getNserie()) != null) {
                throw new PreexistingEntityException("Motos " + motos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Motos motos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            motos = em.merge(motos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = motos.getNserie();
                if (findMotos(id) == null) {
                    throw new NonexistentEntityException("The motos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Motos motos;
            try {
                motos = em.getReference(Motos.class, id);
                motos.getNserie();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The motos with id " + id + " no longer exists.", enfe);
            }
            em.remove(motos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Motos> findMotosEntities() {
        return findMotosEntities(true, -1, -1);
    }

    public List<Motos> findMotosEntities(int maxResults, int firstResult) {
        return findMotosEntities(false, maxResults, firstResult);
    }

    private List<Motos> findMotosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Motos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Motos findMotos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Motos.class, id);
        } finally {
            em.close();
        }
    }

    public int getMotosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Motos> rt = cq.from(Motos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
