/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solemnee3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Draggnel
 */
@Entity
@Table(name = "motos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Motos.findAll", query = "SELECT m FROM Motos m"),
    @NamedQuery(name = "Motos.findByNserie", query = "SELECT m FROM Motos m WHERE m.nserie = :nserie"),
    @NamedQuery(name = "Motos.findByMarca", query = "SELECT m FROM Motos m WHERE m.marca = :marca"),
    @NamedQuery(name = "Motos.findByModelo", query = "SELECT m FROM Motos m WHERE m.modelo = :modelo"),
    @NamedQuery(name = "Motos.findByMotor", query = "SELECT m FROM Motos m WHERE m.motor = :motor"),
    @NamedQuery(name = "Motos.findByValor", query = "SELECT m FROM Motos m WHERE m.valor = :valor")})
public class Motos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nserie")
    private String nserie;
    @Size(max = 2147483647)
    @Column(name = " marca")
    private String marca;
    @Size(max = 2147483647)
    @Column(name = "modelo")
    private String modelo;
    @Size(max = 2147483647)
    @Column(name = "motor")
    private String motor;
    @Size(max = 2147483647)
    @Column(name = "valor")
    private String valor;

    public Motos() {
    }

    public Motos(String nserie) {
        this.nserie = nserie;
    }

    public String getNserie() {
        return nserie;
    }

    public void setNserie(String nserie) {
        this.nserie = nserie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nserie != null ? nserie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Motos)) {
            return false;
        }
        Motos other = (Motos) object;
        if ((this.nserie == null && other.nserie != null) || (this.nserie != null && !this.nserie.equals(other.nserie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.solemnee3.entity.Motos[ nserie=" + nserie + " ]";
    }
    
}
